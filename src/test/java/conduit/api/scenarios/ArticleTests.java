package conduit.api.scenarios;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import conduit.api.steps.ArticleSteps;
import conduit.api.steps.AuthSteps;
import net.serenitybdd.junit5.SerenityJUnit5Extension;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;


@ExtendWith(SerenityJUnit5Extension.class) //Esta anotação indica que os cenários desta classe serão executados utilizando a extensão JUnit5 do Serenity
@WithTags({
    @WithTag("API") //As tags podem ser aplicadas na classe de teste (incluídas em todos os cenários) ou em um cenário específico
})
public class ArticleTests {

    //Estas Strings armazenam os slugs (identificadores) dos artigos criados e utilizados nos cenários de teste
    static String artigoConsultarSlug;
    static String artigoEditarSlug;
    static String artigoRemoverSlug;
    static String artigoCriarComSucessoSlug;

    //As anotações @Steps servem para indicar em quais classes estão declarados os passos que serão utilizados nos cenários
    //Só serão exibidos no relatório os passos das classes marcadas com a anotação @Steps  
    @Steps
    AuthSteps auth;

    @Steps
    ArticleSteps article;

    //A anotação @BeforeAll indica um método que deve ser executado apenas uma vez, antes do início da execução dos cenários dessa classe.
    //Utilizamos a anotação @BeforeAll para a preparação dos cenários de testes. Nesse caso, popular a base de dados com as pré-condições.  
    @BeforeAll
    public static void beforeAll() {
        //Não é possível utilizar a anotação @Steps no método beforeAll, portanto as classes devem ser declaradas pela forma convencional
        AuthSteps auth = new AuthSteps();
        ArticleSteps article = new ArticleSteps();

        auth.fazerLogin();

        //Criar artigo para consulta
        article.informarParametrosDoArtigo(
            "Consultar",
            "Artigo Consultar",
            "Artigo para teste de consulta",
            "#qa"
        );
        article.criarArtigo();
        artigoConsultarSlug = article.obterSlug();

        //Criar artigo para edição
        article.informarParametrosDoArtigo(
            "Editar",
            "Descrição original",
            "Artigo para teste de edição",
            "#qa"
        );
        article.criarArtigo();
        artigoEditarSlug = article.obterSlug();
        
        //Criar artigo para remoção
        article.informarParametrosDoArtigo(
            "Remover",
            "Artigo Remover",
            "Artigo para teste de remoção",
            "#qa"
        );
        article.criarArtigo();
        artigoRemoverSlug = article.obterSlug();         
    }


    @Title("T01: Criar um artigo com sucesso") //Esta anotação define como o título do cenário será exibido no relatório 
    @ParameterizedTest //Esta anotação indica que este cenário contém parâmetros, sendo usado em conjunto com a anotação seguinte.
    //A anotação @CsvSource indica que os parâmetros deste cenário são passados em formato csv.
    //Os dados em csv podem ser passados em um arquivo externo, ou como no caso abaixo, diretamente na classe de teste.      
    @CsvSource(
        value = 
        {"Artigo Best Code | Best Code Qualidade de Software | Teste de criação de artigo através da API do Conduit | #qa,#api"},
        delimiterString = "|"
    )
    //Os parâmetros passados na anotação @CsvSource devem ser incluídos na declartação do método de teste
    public void test01(String titulo, String descricao, String texto, String tags) {
        auth.fazerLogin();
        article.informarParametrosDoArtigo(titulo, descricao, texto, tags);
        article.criarArtigo();
        article.checarStatusCode(201);
        article.checarMensagemRetornada("Created successfully");
        artigoCriarComSucessoSlug = article.obterSlug();
    }


    @Title("T02: Criar um artigo sem informar os parâmetros obrigatórios") 
    @ParameterizedTest
    //Para executarmos o mesmo cenário múltiplas vezes, apenas alterando os parâmetros, podemos informar um csv com várias linhas.
    //Neste caso, utilizando textBlock, temos 4 variações de parâmetros, portanto este cenário será executado 4 vezes.
    @CsvSource(textBlock = """        
         | Best Code Qualidade de Software | Teste de criação de artigo sem título | #qa,#api | title
         Artigo Best Code | | Teste de criação de artigo sem descrição | #qa,#api | description
         Artigo Best Code | Um artigo sem texto | | #qa,#api | body
         Artigo Best Code | Best Code Qualidade de Software | Teste de criação de artigo sem nenhuma tag | | tagList
        """, delimiterString = "|"
    )         
    public void test02(String titulo, String descricao, String texto, String tags, String campoNulo) {
        auth.fazerLogin();
        article.informarParametrosDoArtigo(titulo, descricao, texto, tags);
        article.criarArtigo();
        article.checarStatusCode(500);
        article.checarMensagemRetornada("Field '"+campoNulo+"' doesn't have a default value");
    }


    @Title("T03: Consultar artigo através da slug da URL")   
    @Test       
    public void test03() {
        article.consultarArtigoPelaSlug(artigoConsultarSlug);
        article.checarStatusCode(200);
        article.checarTituloDoArtigoConsultado("Consultar");       
    }
    

    @Title("T04: Editar um artigo com sucesso")  
    @ParameterizedTest    
    @CsvSource(textBlock = """
        Editar Atualizado | Descrição Atualizada | Texto do artigo atualizado | #qa,#api
        """, delimiterString = "|"
    )        
    public void test04(String titulo, String descricao, String texto, String tags) {
        auth.fazerLogin();
        article.informarParametrosDoArtigo(titulo, descricao, texto, tags);
        article.editarArtigo(artigoEditarSlug);
        article.checarStatusCode(200);
        article.checarMensagemRetornada("Updated successfully");
        article.checarTituloDoArtigoEditado(titulo);        
        article.checarDescricaoDoArtigoEditado(descricao);
        article.checarTextoDoArtigoEditado(texto);
        article.checarTagsDoArtigoEditado("#qa");
    }
    

    @Title("T05: Remover um artigo com sucesso")     
    @Test         
    public void test05() {
        auth.fazerLogin();
        article.removerArtigo(artigoRemoverSlug);
        article.checarStatusCode(200);
        article.checarMensagemRetornada("Deleted successfully");       
    }


    //A anotação @AfterAll indica um método que deve ser executado apenas uma vez, após a execução de todos os cenários dessa classe.
    //Podemos utilizar a anotação @AfterAll para limpeza da base de testes. Nesse caso, remover os artigos criados durante a execução dos testes.
    @AfterAll
    public static void afterAll() {
        //Não é possível utilizar a anotação @Steps no método afterAll, portanto as classes devem ser declaradas pela forma convencional
        AuthSteps auth = new AuthSteps();
        ArticleSteps article = new ArticleSteps();

        auth.fazerLogin();
        
        //Remoção dos artigos através dos slugs armazenados
        article.removerArtigo(artigoCriarComSucessoSlug);
        article.removerArtigo(artigoConsultarSlug);
        article.removerArtigo(artigoEditarSlug);
        article.removerArtigo(artigoRemoverSlug);
    }

}
