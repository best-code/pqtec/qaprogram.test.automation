package conduit.api;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

//A classe marcada com a anotação @Suite indica quais testes serão executados por esta Suíte
//Na anotação @SelectPackages, são informados os pacotes contendo as classes de teste que serão executadas
@Suite
@SelectPackages("conduit.api.scenarios")
public class APITestSuite {}
