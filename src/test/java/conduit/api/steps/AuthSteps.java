package conduit.api.steps;

import com.google.gson.JsonObject;
import net.thucydides.core.annotations.Step;

//Nesta classe são declarados os passos relacionados à funcionalidade de Autenticação da API
public class AuthSteps extends CommonSteps{

    //A anotação @Step indica que esse método corresponde a um passo de teste.
    //Apenas os métodos com essa anotação são exibidos no relatório
    //Na anotação @Step é possível passar a descrição do passo, como será exibida no relatório
    @Step("Fazer login como usuário válido")
    public void fazerLogin() {
        //Criação do objeto Json contendo os dados de autenticação
        JsonObject jsonAuth = new JsonObject();
        jsonAuth.addProperty("email", "matheus.kerr@bestcode.com.br");
        jsonAuth.addProperty("password", "best123");

        //Inclusão do objeto Json no corpo da requisição
        request.given()
            .body(jsonAuth);

        //Chamada da API de autenticação e armazenamento do token retornado
        response = request.when()
            .post("/users/login");
        CommonSteps.token = response.getBody().jsonPath().getString("data.token");
    }
    
}
