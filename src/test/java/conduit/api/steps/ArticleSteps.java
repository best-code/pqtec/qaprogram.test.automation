package conduit.api.steps;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.thucydides.core.annotations.Step;

//Nesta classe são declarados os passos relacionados à funcionalidade de Artigos da API
public class ArticleSteps extends CommonSteps{

    //Declaração do objeto Json referente ao corpo da requisição
    JsonObject jsonArtigo = new JsonObject();

    //A anotação @Step indica que esse método corresponde a um passo de teste.
    //Apenas os métodos com essa anotação são exibidos no relatório
    //Na anotação @Step é possível passar a descrição do passo, como será exibida no relatório
    //{x} são substituídos pelo valor do respectivo parâmetro na descrição, onde x é a posição do parâmetro no método
    @Step("Informar os parâmetros de criação do artigo: Título = {0}; Descrição = {1}; Texto = {2}; Tags = {3}")
    public void informarParametrosDoArtigo(String titulo, String descricao, String texto, String tags) {
        //Montagem do Json contendo os dados do artigo
        jsonArtigo.addProperty("title", titulo);
        jsonArtigo.addProperty("description", descricao);
        jsonArtigo.addProperty("body", texto);
        if(tags != null) {
            JsonArray tagList = new JsonArray();
            for (String tag : tags.split(",")) {
                tagList.add(tag);
            }
            jsonArtigo.add("tagList", tagList);
        }

        //Inclusão do objeto Json no corpo da requisição
        request.given()
            .body(jsonArtigo);
    }

    @Step("Criar um novo artigo")
    public void criarArtigo() {
        //Inclusão do token no cabeçalho da requisição
        request.given().header("Authorization", "Bearer "+token);
        //Chamada da API de criação do artigo
        response = request.when()
            .post("/articles");
        
    }

    @Step("Editar o artigo: {0}")
    public void editarArtigo(String slugUrl) {
        //Inclusão do token no cabeçalho da requisição
        request.given().header("Authorization", "Bearer "+token);
        //Chamada da API de edição do artigo
        response = request.when()
            .put("/articles/" + slugUrl);
    }    

    @Step("Consultar o artigo com a slug {0}")
    public void consultarArtigoPelaSlug(String slugUrl) {
        //Chamada da API de consulta do artigo pela slug
        response = request.when()
            .get("/articles/" + slugUrl);
    }

    @Step("Remover o artigo: {0}")
	public void removerArtigo(String slugUrl) {
        //Inclusão do token no cabeçalho da requisição
        request.given().header("Authorization", "Bearer "+token);
        //Chamada da API de remoção do artigo        
        response = request.when()
            .delete("/articles/" + slugUrl);        
	}    

    @Step("Checar se o título do artigo retornado é: {0}")
    public void checarTituloDoArtigoConsultado(String tituloEsperado) {
        //Validação do título retornado na resposta da consulta do artigo
        response.then()
            .body("detailData.title", is(equalTo(tituloEsperado)));
    }

    @Step("Checar se o título do artigo retornado é: {0}")
    public void checarTituloDoArtigoEditado(String tituloEsperado) {
        //Validação do título retornado na resposta da edição do artigo
        response.then()
            .body("data.title", is(equalTo(tituloEsperado)));
    }    

    @Step("Checar se a descrição do artigo retornado é: {0}")    
    public void checarDescricaoDoArtigoEditado(String descricaoEsperada) {
        //Validação da descrição retornado na resposta da edição do artigo
        response.then()
            .body("data.description", is(equalTo(descricaoEsperada)));
    }
    
    @Step("Checar se o texto do artigo retornado é: {0}")
    public void checarTextoDoArtigoEditado(String textoEsperado) {
        //Validação do texto retornado na resposta da edição do artigo
        response.then()
            .body("data.body", is(equalTo(textoEsperado)));
    }

    @Step("Checar se o artigo retornado possui a(s) tag(s): {0}")
    public void checarTagsDoArtigoEditado(String tagsEsperadas) {
        //Geração de uma lista das tags passadas por parâmetro
        List<String> tagList = new ArrayList<>();
        if(tagsEsperadas != null){
            for (String tag : tagsEsperadas.split(",")) {
                tagList.add(tag);
            }            
        }
        //Validação das tags retornadas na resposta da edição do artigo
        response.then()
            .body("data.tagList", equalTo(tagList));
    }

    public String obterSlug() {
        //Extração da slug do artigo retornada na resposta das requisições da API 
        return response.body().jsonPath().getString("data.slug");
    }


}