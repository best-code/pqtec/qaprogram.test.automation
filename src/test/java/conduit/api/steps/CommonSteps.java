package conduit.api.steps;

import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.CoreMatchers.*;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;

//Esta classe que contém os métodos e atributos comuns para todas as requisições, para facilitar a reutilização de código.
//Todas as classes de passos (Steps) devem extender esta classe.
public class CommonSteps {

    RequestSpecification request; //Objeto que contém os parâmetros da requisição que será enviada
    Response response; //Objeto que contém os dados retornados pela requisição após seu envio
    static String token; //Token de autenticação que será passado nas requisições

    //No método construtor da classe, são feitas as configurações básicas e comuns para todas as requisições
    public CommonSteps() {
        RestAssured.baseURI = "http://localhost/api";
        RestAssured.port = 3333;
        request =        
            given()
            .config(RestAssured.config()
            .objectMapperConfig(new ObjectMapperConfig(ObjectMapperType.GSON)))
            //.log().all()
            .contentType("application/json");
    }

    //O Status Code é um atributo comum para as respostas de todas as requisições da API, portanto sua validação pode ser realizada na classe comum
    @Step("Checar se o status code retornado é: {0}")
    public void checarStatusCode(int statusCode) {
        response.then()
            .statusCode(equalTo(statusCode));
    }

    //A mensagem é um atributo comum para as respostas de todas as requisições da API, portanto sua validação pode ser realizada na classe comum
    @Step("Checar se a mensagem retornada contém o texto: {0}")
    public void checarMensagemRetornada(String messageString) {
        response.then()
            .body("message", containsString(messageString));
    }

}