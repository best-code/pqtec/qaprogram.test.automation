#language: pt

Funcionalidade: Ler Artigo
Como leitor de artigos
Quero poder ler os artigos publicados plataforma Conduit
Para me informar sobre assuntos do meu interesse

Critérios de aceitação:
- o leitor deve poder acessar os artigos publicados, mesmo sem estar logado (apenas para visualização)
- o leitor deve poder buscar os artigos pelas tags, mesmo sem estar logado
- o leitor deve estar logado para poder postar comentários nos artigos e apagar seus próprios comentários
- o leitor deve estar logado para poder seguir o autor e adicionar um artigo aos favoritos

Cenário: Visualizar Artigo
    Dado o leitor deseja "visualizar" um artigo
    Quando o leitor acessa a Plataforma ''

Cenário: Postar Comentário

Cenário: Postar Comentário sem estar logado na plataforma

Cenário: Apagar Comentário

Cenário: Apagar Comentário sem estar logado na plataforma

Cenário: Seguir Autor

Cenário: Seguir Autor sem estar logado na plataforma

Cenário: Adicionar aos Favoritos 

Cenário: Adicionar aos Favoritos sem estar logado na plataforma

Cenário: Buscar Artigos pelas Tags