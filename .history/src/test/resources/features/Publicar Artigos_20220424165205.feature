#language: pt

Funcionalidade: Publicar Artigo
Como autor de artigos 
Quero poder publicar artigos na plataforma Conduit 
Para compartilhar conhecimento com outras pessoas em um ambiente online

Critérios de aceitação:
- o autor deve poder publicar artigos, assim como editar e remover seus próprios artigos
- Título, Descrição, Texto e uma Tag devem ser informações obrigatórias para publicar um artigo
- o Título do artigo não pode ser repetido
- o autor deve estar logado na plataforma para poder publicar artigos

@manual-result:success
Cenário: Publicar um Artigo com sucesso
[Ver video](assets/PublicarArtigo.webm)
    Dado o autor estar logado na Plataforma "Conduit"
    Quando o autor criar um artigo com as seguintes informações
            | Titulo   | Descrição                           | Texto                                           | Tags |
            | BestCode | BestCode é uma empresa de qualidade | BestCode é uma empresa de qualidade de software | #01  |
    Então o sistema exibe a mensagem informando "Creadted sucessfully"

@manual@manual-result:failed
@manual-last-tested:01/04/2022
@manual-test-evidence:assets/error_PublicarArtigoValorNaoPreenchido.jpg
Esquema do Cenário: Publicar um Artigo com os campos não preenchidos
    Dado o autor estar logado na Plataforma "Conduit"
    Quando o autor criar um artigo com as seguintes informações não preenchidas
            | <Titulo> | <Descrição> | <Texto> | <Tags> |
    Então o sistema retorna a mensagem "This field is required" para cada campo não preenchido
    Exemplos:
            | Titulo   | Descrição | Texto    | Tags |
            |          |           | BestCode |      |
            | BestCode |           |          |      |
            |          |           |          |  #01 | 

@manual-result:success
Cenário: Publicar Artigo com título repetido
[Ver imagem](assets/TituloRepetidoArtigo.png)
    Dado o autor estar logado na Plataforma "Conduit"
    Quando o autor criar um novo artigo repetindo o "Titulo" de um artigo publicado
            | Artigo publicado | Artigo a criar |
            | Teste            | Teste          |
    Então o sistema retorna a mensagem "title must be unique"

@manual-result:success
Cenário: Editar um Artigo
[Ver video](assets/EditarArtigo.webm)
        Dado o autor estar logado na Plataforma "Conduit"
        Quando o autor edita o artigo publicado  
                        | Status           | Titulo      | Descrição                           | Texto                                           | Tags |
                        | Artigo publicado | BestCode    | BestCode é uma empresa de qualidade | BestCode é uma empresa de qualidade de software | #01  |
                        | Artigo editado   | QA_BestCode | BestCode é uma empresa de qualidade | BestCode é uma empresa de qualidade de software | #Qa  |
        Então o sistema subistitui o valor do artigo publicado pelo valor do artigo editado

@manual@manual-result:failed
@manual-last-tested:01/04/2022
@manual-test-evidence:assets/error_EditarValornaoPreenchido.webm 
Cenário: Editar Artigo com valores não preenchidos
        ado o autor estar logado na Plataforma "Conduit"
    Quando o autor edita um artigo publicado com as seguintes informações não preenchidas
                | <Titulo> | <Descrição> | <Texto> | <Tags> |
    Então o sistema retorna a mensagem "This field is required" para cada campo não preenchido
    Exemplos:
                | Status           | Titulo   | Descrição                           | Texto                                           | Tags |
                | Artigo publicado | BestCode | BestCode é uma empresa de qualidade | BestCode é uma empresa de qualidade de software | #01  |
                | Artigo editado   |          |                                     |                                                 |      |

@manual-result:success
Cenário: Remover um Artigo
[Ver video](assets/RemoverArtigo.webm)
    Dado o autor estar logado na Plataforma "Conduit"
    Quando o autor aciona para "remover" um artigo
    Então o artigo é "apagado" pelo sistema