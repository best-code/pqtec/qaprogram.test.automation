#language: pt

Funcionalidade: Publicar Artigo
Como autor de artigos 
Quero poder publicar artigos na plataforma Conduit 
Para compartilhar conhecimento com outras pessoas em um ambiente online

Critérios de aceitação:
- o autor deve poder publicar artigos, assim como editar e remover seus próprios artigos
- Título, Descrição, Texto e uma Tag devem ser informações obrigatórias para publicar um artigo
- o Título do artigo não pode ser repetido
- o autor deve estar logado na plataforma para poder publicar artigos

Cenário: Publicar um Artigo com sucesso
    Dado o autor estar logado na Plataforma "Conduit"
    Quando o autor criar um artigo com as seguintes informações:
            | Titulo   | Descrição                           | Texto                                           | Tags |
            | BestCode | BestCode é uma empresa de qualidade | BestCode é uma empresa de qualidade de software | #01  |

Esquema do Cenário: Publicar um Artigo com os campos não preenchidos
    Quando o autor criar um artigo com as seguintes informações não preenchidas:
            | Titulo   | Descrição                           | Texto                                           | Tags |

Cenário: Publicar Artigo com título repetido

Cenário: Editar um Artigo

Cenário: Remover um Artigo