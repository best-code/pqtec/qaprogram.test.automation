#language: pt

Funcionalidade: Ler Artigo
Como leitor de artigos
Quero poder ler os artigos publicados plataforma Conduit
Para me informar sobre assuntos do meu interesse

Critérios de aceitação:
- o leitor deve poder acessar os artigos publicados, mesmo sem estar logado (apenas para visualização)
- o leitor deve poder buscar os artigos pelas tags, mesmo sem estar logado
- o leitor deve estar logado para poder postar comentários nos artigos e apagar seus próprios comentários
- o leitor deve estar logado para poder seguir o autor e adicionar um artigo aos favoritos

@manual-result:success
Cenário: Visualizar Artigo - Não logado
[Ver imagem](assets/Leitor_visualiarArtigo.png)
    Dado o leitor não logado deseja "visualizar" um artigo publicado na Plataforma "Conduit"
    Quando o leitor acessa a Plataforma "Conduit" sem fazer "Login"
    Então o sistema exibe os artigos publicados
    Então o leitor pode visualizar os artigos publicados 

@manual-result:success
Cenário: Visualizar Artigo - Logado
    Dado o leitor logado deseja "visualizar" um artigo publicado na Plataforma "Conduit"
    Quando o leitor acessa a Plataforma "Conduit" 
    E faz o "Login"
    Então o sistema exibe os artigos publicados
    Então o leitor pode visualizar, postar coemtario e excluir os artigos publicados

@manual-result:success
Cenário: Postar Comentário - Não logado 
[Ver imagem](assets/Leitor_PostarComentario.png)
    Dado o leitor não logado deseja "Postar Comentario" em um artigo na Plataforma "Conduit"
    Quando o leitor acessa o artigo desejado para postar um "Comentario"
    Então o sistema retorna a mensagem informando "Sign in or sign up to add comments on this article." 

@manual-result:success
Cenário: Postar Comentário - Logado
[Ver video](assets/Leitor_postarcoemtarioLOGADO.webm)
    Dado o leitor estar logado na Plataforma "Conduit"
    Quando o leitor aciona para postar o "comentario" em um artigo desejado
    Então o sistema publica o comentario

@manual-result:success
Cenário: Apagar Comentário
[Ver video](assets/Leitor_apagarcoemtario.webm)
    Dado o leitor postou um comentario em um artigo
    Quando o leitor aciona para "apagar" o comentario
    Então o sistema apaga o comentario

@manual-result:success
Cenário: Seguir Autor - Logado
[Ver video](assets/Leitor_seguir.webm)
    Dado o leitor estar na tela de um artigo criado
    Quando ele aciona para seguir o "Autor"
    Então o sistema segue o autor

Cenário: Seguir Autor - Não logado
[Ver video](assets/Leitor_seguirAutor_naoLogado.webm)
    Dado o leitor estar na tela de um artigo criado
    Quando ele aciona para seguir o "Autor"
    Então o sistema segue o autor

Cenário: Adicionar aos Favoritos 

Cenário: Adicionar aos Favoritos sem estar logado na plataforma

Cenário: Buscar Artigos pelas Tags